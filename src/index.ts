import { sync as commandExistsSync } from 'command-exists';

/**
 * Check if Yarn is available.
 *
 * @exports
 * @returns {boolean}
 */
function caniuseYarn(): boolean {
	return commandExistsSync('yarn');
}

export = caniuseYarn;
