#!/usr/bin/env node

import chalk from 'chalk';
import { cross, tick } from 'figures';
import { EOL } from 'os';
import yargs from 'yargs';

import caniuseYarn from './index';

const { cyan, green, red } = chalk;
const yarn = cyan('yarn');

yargs
	.scriptName(cyan('caniuse-yarn'))
	.help()
	.version()
	.alias('help', 'h')
	.alias('version', 'v')
	.usage('$0', 'Check if Yarn is available.').argv;

if (caniuseYarn()) {
	process.stdout.write(
		green(`${tick} ${yarn} is available. You can use ${yarn}.${EOL}`)
	);
} else {
	process.stdout.write(
		red(`${cross} ${yarn} is not available! You cannot use ${yarn}.${EOL}`)
	);
}
