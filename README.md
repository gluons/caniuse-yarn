# caniuse-yarn
[![NPM](https://img.shields.io/npm/l/caniuse-yarn.svg?style=flat-square)](https://gitlab.com/gluons/caniuse-yarn/blob/master/LICENSE)
[![npm](https://img.shields.io/npm/v/caniuse-yarn.svg?style=flat-square)](https://www.npmjs.com/package/caniuse-yarn)
[![npm](https://img.shields.io/npm/dt/caniuse-yarn.svg?style=flat-square)](https://www.npmjs.com/package/caniuse-yarn)
![npm type definitions](https://img.shields.io/npm/types/caniuse-yarn.svg?style=flat-square)
[![pipeline status](https://gitlab.com/gluons/caniuse-yarn/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/gluons/caniuse-yarn/pipelines)

Check if [Yarn](https://yarnpkg.com/) is available.

## Installation

```bash
npm install caniuse-yarn
# or
yarn add caniuse-yarn
# or
yarn install caniuse-yarn
```

## CLI

You can also install as global package to use CLI.

```bash
npm install -g caniuse-yarn
# or
yarn global add caniuse-yarn
```

```
caniuse-yarn

Check if Yarn is available.

Options:
  --help, -h     Show help                                             [boolean]
  --version, -v  Show version number                                   [boolean]
```

## Usage

```js
const caniuseYarn = require('caniuse-yarn');

if (caniuseYarn()) {
	console.log('Yarn is available. You can use Yarn.');
} else {
	console.log('Yarn is not available! You cannot use Yarn.');
}
```

## Related

- [caniuse-pnpm](https://gitlab.com/gluons/caniuse-pnpm) - Check if **pnpm** is available.
- [has-yarn](https://github.com/sindresorhus/has-yarn) - Check if a project is using **Yarn**.
- [has-pnpm](https://gitlab.com/gluons/has-pnpm) - Check if a project is using **pnpm**.
